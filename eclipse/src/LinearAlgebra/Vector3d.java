//Caelan Whitter 1841768

package LinearAlgebra;

public final class Vector3d {
	
	private final double x;
	private final double y;
	private final double z;
	
	  public Vector3d (double x, double y, double z)
	  {
		  this.x = x;
		  this.y = y;
		  this.z = z;
	  }
	  
	  public double getX()
	  {
		  return this.x;
	  }
	  
	  public double getY()
	  {
		  return this.y;
	  }
	  
	  public double getZ()
	  {
		  return this.z;
	  }
	  
	  public double magnitude()
	  {
		  double magnitude = Math.sqrt((x*x)+(y*y)+(z*z));
		  return magnitude;
	  }
	  
	  public double dotProduct(Vector3d vector2)
	  {
		  double dotP = (this.getX()*vector2.getX())+(this.getY()*vector2.getY())+(this.getZ()*vector2.getZ());
		  return dotP;
	  }
	  
	  public Vector3d add(Vector3d vector2)
	  {
		  double addedX=this.getX()+vector2.getX();
		  double addedY=this.getY()+vector2.getY();
		  double addedZ=this.getZ()+vector2.getZ();

		  Vector3d vector3 = new Vector3d(addedX,addedY,addedZ);
		  
		  
		 return vector3;
		  
		
	  }
	  
}
