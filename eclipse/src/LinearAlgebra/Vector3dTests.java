//Caelan Whitter 1841768

package LinearAlgebra;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Vector3dTests {

	@Test
	public void testGetMethods() {
		Vector3d vector4 = new Vector3d(10,7,30);
		double result = vector4.getX();
		double result2 = vector4.getY();
		double result3 = vector4.getZ();
		assertEquals(10,result);
		assertEquals(7,result2);
		assertEquals(30,result3);
	}

	
	@Test
	public void testMagnitude() {
		Vector3d vector5 = new Vector3d(7,31,2);
		double result = vector5.magnitude();
		assertEquals(31.843366656181317,result);
	}
	
	
	@Test
	public void testDotProduct() {
		Vector3d vector6 = new Vector3d(1,14,22);
		Vector3d vector7 = new Vector3d(35,66,8);
		
		double result = vector6.dotProduct(vector7);
		assertEquals(1135,result);
		
	}
	
	@Test
	public void testAdd() {
		Vector3d vector8 = new Vector3d(3,21,5);
		Vector3d vector9 = new Vector3d(6,25,1);
		
		Vector3d newVector = vector8.add(vector9);
		
		double result = newVector.getX();
		double result2 = newVector.getY();
		double result3 = newVector.getZ();
		assertEquals(9,result);
		assertEquals(46,result2);
		assertEquals(6,result3);

	}
	

}
